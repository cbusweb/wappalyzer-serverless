module.exports = {
    testEnvironment: 'node',
    'rootDir': 'tests',
    'coveragePathIgnorePatterns': [
        '/node_modules/'
    ],
    'setupFilesAfterEnv': [
        './setup.js'
    ]
}
