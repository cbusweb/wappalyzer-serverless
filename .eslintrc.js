module.exports = {
    root: true,
    extends: '@serverless/eslint-config/node',
    env: { es6: true },
    parserOptions: {
        ecmaVersion: 11,
        sourceType: 'module'
    },
    plugins: ['import'],
    rules: {
        'object-shorthand': 0,
        'prefer-template': 0,
        'no-prototype-builtins': 0,
        'quotes': 0
    }
}
