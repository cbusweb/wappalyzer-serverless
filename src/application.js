import express from 'express';
import container from './dependencies';

const app = express();
const bodyParser = require('body-parser');

// Add middleware.
app.use(bodyParser.json());
app.use(container.ExpressLogger);

// Analyze previously fetched response data.
app.post('/analyze', async (request, response) => {
    return container.Controller.analyze(request, response);
});

// Return a list of supported categories.
app.get('/categories', async (request, response) => {
    return container.Controller.categories(request, response);
});

// Scan a website and analyze the result.
app.get('/scan', async (request, response) => {
    return container.Controller.scan(request, response);
});

// Return a list of supported technologies.
app.get('/technologies', async (request, response) => {
    return container.Controller.technologies(request, response);
});

// Handle in-valid route
app.all('*', (req, res) => {
    const response = { data: null, message: 'Route not found!!' }
    res.status(404).send(response)
});

// Export the API.
module.exports = app;