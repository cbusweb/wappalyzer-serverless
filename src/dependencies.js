// Import NPM dependencies.
import Bottle from 'bottlejs';
import dotenv from 'dotenv';

// Import application dependencies.
import Scanner from './scanner';

// Add controllers.
import Controller from './controller';

// Add various dependencies.
const expressWinston = require('express-winston');
const winston = require('winston');
const got = require('got');
const wappalyzer = require('simple-wappalyzer');

// Initialize environment.
dotenv.config();

// Create DI container.
const bottle = new Bottle();

bottle.factory('Logger', () => {
    return winston.createLogger({
        levels: winston.config.syslog.levels,
        throwHttpErrors: false,
        format: winston.format.combine(
            winston.format.json(),
            winston.format.errors({ stack: true })
        ),
        transports: [
            new winston.transports.Console({ level: process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'info' }),
        ]
    });
});

bottle.factory('ExpressLogger', (container) => {
    return expressWinston.logger({
        winstonInstance: container.Logger,
    });
});

bottle.factory('Scanner', (container) => {
    return new Scanner ({
        logger: container.Logger,
        httpClient: got,
        lambda: container.Lambda,
        wappalyzer: wappalyzer,
    });
});

bottle.factory('Controller',  (container) => {
    return new Controller ({
        logger: container.Logger,
        httpClient: got,
        scanner: container.Scanner,
    });
});

export default bottle.container;