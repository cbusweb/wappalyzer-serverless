/**
 * Provides CRUD endpoints for Qualtrics Responses.
 */
export default class Controller  {

    /**
     * Construct the controller.
     * @param options
     */
    constructor(options) {
        this.httpClient = options.httpClient;
        this.logger = options.logger;
        this.scanner = options.scanner;
    }

    ok(response, data) {
        return response.status(200).send(data);
    }

    error(response, data) {
        return response.status(500).send(data);
    }

    /**
     * Retrieve a response.
     * @param request
     * @param response
     * @returns {Promise<*>}
     */
    async analyze(request, response) {
        this.logger.info('Controller::analyze(' + request.query.url + ')');
        const result = await this.scanner.analyze(request.body.url, request.body.statusCode, request.body.headers, request.body.html);
        return this.ok(response, result);
    }

    /**
     * Retrieve a response.
     * @param request
     * @param response
     * @returns {Promise<*>}
     */
    async categories(request, response) {
        this.logger.info('Controller::categories()');
        const result = await this.scanner.categories();
        return this.ok(response, result);
    }

    /**
     * Retrieve a response.
     * @param request
     * @param response
     * @returns {Promise<*>}
     */
    async scan(request, response) {
        this.logger.info('Controller::scan(' + request.query.url + ')');
        const result = await this.scanner.scan(request.query.url);
        return this.ok(response, result);
    }

    /**
     * Retrieve a response.
     * @param request
     * @param response
     * @returns {Promise<*>}
     */
    async technologies(request, response) {
        this.logger.info('Controller::technologies()');
        const result = await this.scanner.technologies();
        return this.ok(response, result);
    }
}

