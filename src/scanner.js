export default class Scanner {

    /**
     * Creates a Scanner
     * @param options
     */
    constructor(options) {
        this.logger = options.logger;
        this.httpClient = options.httpClient;
        this.wappalyzer = options.wappalyzer;
    }

    /**
     * Analyze response data to detect applications.
     * @param url
     * @param statusCode
     * @param headers
     * @param html
     * @returns {Promise<null|*>}
     */
    async analyze(url, statusCode, headers, html) {
        this.logger.info(`Scanner::analyze(${ url})`);
        try {
            const results =  await this.wappalyzer({
                url: url,
                html: html,
                statusCode: statusCode,
                headers: headers
            });
            this.logger.info(`Wappalyzer detected ${results.length} items.`);
            return results
        }
        catch(error) {
            this.logger.error(`Failed wappalyzing ${url}`, error);
        }
        return null;
    }

    /**
     * Request a url and analyze the response to detect applications.
     * @param url
     * @returns {Promise<null|*>}
     */
    async scan(url) {
        this.logger.info(`Scanner::scan(${ url})`);

        try {
            const response = await this.httpClient.get(url, { throwHttpErrors: true });
            this.logger.info(`get(${url}) received status ${response.status} with length ${response.body.length}.`);
            return await this.analyze(url, response.statusCode, response.headers, response.body);
        }
        catch (error) {
            this.logger.error(`Failed fetching ${url}`, error);
        }
        return null;
    }

    /**
     * Return categories detected by the scanner.
     * @returns {Promise<void>}
     */
    async categories() {
        const results = {};
        const json = require('../node_modules/simple-wappalyzer/src/apps.json');
        // eslint-disable-next-line
        for (const id in json.categories) {
            results[id] = json.categories[id].name;
        }
        return results;
    }

    /**
     * Return technologies detected by the scanner.
     * @returns {Promise<void>}
     */
    async technologies() {
        const results = {};
        const json = require('../node_modules/simple-wappalyzer/src/apps.json');

        // eslint-disable-next-line
        for (const id in json.apps) {
            const categories = {};
            for (let i=0; i<json.apps[id].cats.length; i++) {
                const catId = json.apps[id].cats[i];
                categories[catId] = json.categories[catId].name;
            }

            results[id] = {
                icon: json.apps[id].icon,
                categories: categories
            };
        }
        return results;
    }
}