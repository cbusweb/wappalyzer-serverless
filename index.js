/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);

const serverless = require('serverless-http');
const app = require('./src/application.js');

module.exports.handler = serverless(app);

