# Scanner

A Wappalyzer based scanner of websites in Lambda using jsdom instead of headless chrome.

## Caveat
Jsdom is easier to run in lambda than headless chrome but comes with 
[security tradeoffs](https://github.com/jsdom/jsdom#executing-scripts)
in that in some scenarios a site's scripts can be executed and escape 
jsdom's containment. You should treat this lambda as an untrusted sandbox, 
particularly when analyzing untrusted sites.

## Local Development

### Dependencies

node v12 required to run the app.
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
nvm install 12
```

serverless (required) for any serverless framework based lambda work.
```
curl -o- -L https://slss.io/install | bash
```

jq (recommended) for pretty printing json responses from curl. 
```
brew install jq
```

### Quick start

```
# Install main project dependencies.
npm ci

# Launch serverless offline to simulate AWS environment.
npm start

# Fetch a response over the api.
curl http://localhost:3001/dev/scan?url=https://drupal.org | jq

# Fetch all categories
curl http://localhost:3001/dev/categories | jq

# Fetch all technologies
curl http://localhost:3001/dev/technologies | jq
```

## Deployment

For deployment, we use Gitlab CI/CD with a separate AWS account for testing and production.

The default settings in serverless.yml are setup for the test environment.

Use something like this to force deploy to test.
```
serverless deploy --aws-profile cbus.sites.test
```

If setting up CD, you'll need to do a few things.
* Setup route53 zone in advance.
* Setup ACM in advance.
* Configure the CERT_NAME and DOMAIN environment variables.