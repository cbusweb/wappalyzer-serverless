/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);

// Load supertest.
const supertest = require('supertest');

// Load our application.
const app = require('../src/application');

describe ('Application', () => {

    describe ('GET /categories',  () => {
        it('responds responds with many categories', async (done) => {
            const result = await supertest(app).get('/categories');
            expect(result.statusCode).toBe(200);
            expect(Object.getOwnPropertyNames(result.body).length).toBeGreaterThan(50);
            expect(result.body["1"]).toBe('CMS');
            done();
        });
    });

    describe ('GET /technologies',  () => {
        it('responds responds with many technologies', async (done) => {
            const result = await supertest(app).get('/technologies');
            expect(result.statusCode).toBe(200);
            expect(Object.getOwnPropertyNames(result.body).length).toBeGreaterThan(500);

            const drupal = result.body.Drupal;
            expect(drupal.icon).toBe('Drupal.svg');
            expect(drupal.categories['1']).toBe('CMS');
            done();
        });
    });

    describe('POST /analyze', () => {
        it('responds responds with many technologies', async (done) => {

            // This should register as Drupal and an implied PHP.
            const drupal = {
                url: 'https://drupal.org',
                statusCode: 200,
                headers: {
                    "Expires": "19 Nov 1978",
                    "X-Drupal-Cache": "",
                    'x-generator': 'Drupal 7 (https://www.drupal.org)',
                },
                html: '<html><head></head><body></body></html>'
            };

            const result = await supertest(app).post('/analyze').send(drupal);

            expect(result.statusCode).toBe(200);
            expect(result.body.length).toBe(2);
            expect(result.body[0].slug).toBe('drupal');
            done();
        });
    });
});
